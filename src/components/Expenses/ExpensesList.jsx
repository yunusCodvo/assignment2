import React from 'react';
import ExpenseItem from './ExpenseItem';
import './ExpenseList.css';
const ExpensesList = ({ filteredExpenses }) => {
  if (filteredExpenses?.length == 0) {
    return <h3 className="expenses-list__fallback">Expense not found</h3>;
  }
  console.log('List filter', filteredExpenses);
  return (
    <ul className="expenses-list">
      {filteredExpenses?.map((item) => (
        <ExpenseItem expenseAll={item} key={item.id} />
      ))}
    </ul>
  );
};

export default ExpensesList;
