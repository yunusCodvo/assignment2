import React from 'react';

import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = ({ onSaveExpense }) => {
  const onAddExpense = (expense) => {
    const objExp = {
      ...expense,
      id: Math.random().toString(),
    };
    onSaveExpense(objExp);
  };
  return (
    <div className="new-expense">
      <ExpenseForm onAddExpense={onAddExpense} />
    </div>
  );
};

export default NewExpense;
